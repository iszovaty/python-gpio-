import RPi.GPIO as GPIO
import time
#GPIO module name provided
#GPIO.BOARD - Board numbering scheme. The pin numbers follow the pin numbers on header P1.
#GPIO.BCM   -  Broadcom chip-specific pin numbers. These pin numbers follow the lower-level numbering system defined by the Raspberry Pi’s Broadcom-chip brain.

butPin = 17
pwmSzervoPin = 18
pwmBasicMotorPin = 19
dutyCycle = 50

GPIO.setmode(GPIO.BCM)

#Setting up PIN mode

GPIO.setup(pwmSzervoPin, GPIO.OUT)
GPIO.setup(pwmBasicMotorPin, GPIO.OUT)
GPIO.setup(butPin, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Button pin set as input w/ pull-up

#To write a pin high or low, use the GPIO.output([pin], [GPIO.LOW, GPIO.HIGH]) function. 
#For example, if you want to set pin 18 high, write:
#GPIO.output(18, GPIO.HIGH)

#puts 3.3V to theese pin (LOW would set it to 0v)
GPIO.output(pwmSzervoPin, GPIO.HIGH)
GPIO.output(pwmBasicMotorPin, GPIO.HIGH)


#sending pwm signals (Analog signals)
pwm = GPIO.PWM(pwmSzervoMotorPin, 1000)
pwm.start(dutyCycle) # with this, the pwm adjust value will be set on 50%

print("Here we go! Press CTRL+C to exit")

try:
	while :
		if GPIO.input(butPin): #button is released
			pwm.ChangeDutyCycle(dc)
		else:
			
except KeyboardInterrupt:
	pwm.stop()
	GPIO.cleanup()	
